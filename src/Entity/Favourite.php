<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FavouriteRepository")
 */
class Favourite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $redditName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", length=255)
     */
    private $dateTime;

    public function __construct()
    {
        $this->dateTime = new \DateTime('now', new \DateTimeZone('Asia/Bishkek'));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRedditName(): ?string
    {
        return $this->redditName;
    }

    public function setRedditName(string $redditName): self
    {
        $this->redditName = $redditName;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }
}
