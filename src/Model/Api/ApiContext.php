<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{

    /**
     * @param $data
     * @return array
     * @throws ApiException
     */
    public function getReddit($data)
    {
        return $this->makeQuery('https://www.reddit.com/r/picture/search.json', self::METHOD_GET, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getCurrentReddit($data)
    {
        return $this->makeQuery('https://www.reddit.com/api/info.json', self::METHOD_GET, $data);
    }

}
