<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => '123@123.ru',
            'password' => '123',
            'name' => 'Dyadya Vasya'
        ]);

        $manager->persist($user);

        $user = $this->userHandler->createNewUser([
            'email' => '321@123.ru',
            'password' => '123',
            'name' => 'Dza Vsa'
        ]);
        $manager->persist($user);
        $user = $this->userHandler->createNewUser([
            'email' => '123@321.ru',
            'password' => '123',
            'name' => 'Gregor Makgregor'
        ]);

        $manager->persist($user);
        $manager->flush();


    }
}
