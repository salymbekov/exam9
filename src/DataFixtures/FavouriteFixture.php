<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Favourite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FavouriteFixture extends Fixture
{


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 4; $i++){
            $favourite = new Favourite();
            $favourite->setUser($i);
            $favourite->setRedditName('t3_8wgone');
            $manager->persist($favourite);

            $favourite = new Favourite();
            $favourite->setUser($i);
            $favourite->setRedditName('t3_8qaaoq');
            $manager->persist($favourite);

            $favourite = new Favourite();
            $favourite->setUser($i);
            $favourite->setRedditName('t3_8widua');
            $manager->persist($favourite);

            $favourite = new Favourite();
            $favourite->setUser($i);
            $favourite->setRedditName('t3_7x1mc1');
            $manager->persist($favourite);

            $favourite = new Favourite();
            $favourite->setUser($i);
            $favourite->setRedditName('t3_8talwl');
            $manager->persist($favourite);

            $manager->flush();
        }

    }
}
