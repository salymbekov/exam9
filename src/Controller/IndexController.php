<?php

namespace App\Controller;

use App\Entity\Favourite;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\FavouriteRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/profile", name="profile")
     * @param FavouriteRepository $favouriteRepository
     * @param ApiContext $apiContext
     * @param Request $request
     * @return Response
     * @throws ApiException
     */
    public function profileAction(
        FavouriteRepository $favouriteRepository,
        ApiContext $apiContext,
        Request $request
    ) {
        if (!empty($request->get('page'))) {
            $offset = ($request->get('page') - 1) * 2;
        } else {
            $offset = 0;
        }
        $posts = $favouriteRepository->findByUserForPagination($this->getUser()->getId(), $offset);
        $qty = $favouriteRepository->countPostsForUser($this->getUser()->getId());

        $pages = ceil($qty[0]['qty'] / 2);

        $id = '';
        foreach ($posts as $post) {
            $id .= $post->getRedditName() . ',';
        }
        $reddit = $apiContext->getCurrentReddit(['id' => $id]);
        return $this->render('profile.html.twig', [
            'results' => $reddit['data']['children'],
            'pages' => $pages

        ]);
    }

    /**
     * @Route("/society-profile", name="society-profile")
     * @param FavouriteRepository $favouriteRepository
     * @param ApiContext $apiContext
     * @param Request $request
     * @return Response
     * @throws ApiException
     */
    public function societyProfileAction(
        FavouriteRepository $favouriteRepository,
        ApiContext $apiContext,
        Request $request
    ) {
        if (!empty($request->get('page'))) {
            $offset = ($request->get('page') - 1) * 2;
        } else {
            $offset = 0;
        }
        $posts = $favouriteRepository->societyFavourite($offset);
        $qty = $favouriteRepository->countAllPosts();
        $pages = ceil(count($qty) / 2);
        $id = '';
        foreach ($posts as $post) {
            $id .= $post['redditName'] . ',';
        }
        $reddit = $apiContext->getCurrentReddit(['id' => $id]);
        return $this->render('societyProfile.html.twig', [
            'results' => $reddit['data']['children'],
            'pages' => $pages,
            'qty' => $qty
        ]);
    }


    /**
     * @Route("/", name="home-page")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function indexAction(
        Request $request,
        ApiContext $apiContext
    ) {
        $results = null;
        $form = $this->createForm('App\Form\RedditType');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $results = $apiContext->getReddit([
                'q' => $data['search'],
                'sort' => $data['sort'],
                'limit' => $data['limit'],
                'type' => 'link'
            ]);
        }

        return $this->render('home.html.twig', [
            'results' => $results['data']['children'],
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/detail/{name}", name="detail")
     * @param string $name
     * @param ApiContext $apiContext
     * @param Request $request
     * @param FavouriteRepository $favouriteRepository
     * @return Response
     * @throws ApiException
     */
    public function detailAction(
        string $name,
        ApiContext $apiContext,
        Request $request,
        FavouriteRepository $favouriteRepository
    ) {
        $qty = $favouriteRepository->countPostsForUser($name);
        $error = $request->query->get('error');
        $reddit = $apiContext->getCurrentReddit(['id' => $name]);
        return $this->render('detail.html.twig', [
            'results' => $reddit['data']['children'],
            'error' => $error,
            'qty' => $qty[0]
        ]);
    }

    /**
     * @Route("/favourite/{name}", name="favourite")
     * @param string $name
     * @param ObjectManager $objectManager
     * @param FavouriteRepository $favouriteRepository
     * @return Response
     */
    public function addToFavouriteAction(
        string $name,
        ObjectManager $objectManager,
        FavouriteRepository $favouriteRepository
    ) {
        $error = null;
        $result = $favouriteRepository->findOneByUserAndName($this->getUser()->getId(), $name);
        if (empty($result)) {
            $favourite = new Favourite();
            $favourite->setRedditName($name);
            $favourite->setUser($this->getUser()->getId());
            $objectManager->persist($favourite);
            $objectManager->flush();
        } else {
            $error = 'Уже добавлено в избраное';
        }
        return $this->redirectToRoute(
            'detail', ['name' => $name, 'error' => $error]);
    }

}
