<?php

namespace App\Controller;

use App\Entity\Favourite;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\FavouriteRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthComtrollerController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    ) {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = [
                'email' => $user->getEmail(),
                'name' => $user->getName(),
                'password' => $user->getPassword(),
                'roles' => $user->getRoles(),
            ];

            try {
                $user = $userHandler->createNewUser($data);
            } catch (ApiException $e) {
                $error = 'Что-то пошло не так!!!';
            }
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute("home-page");

        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return Response
     */
    public
    function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler
    ) {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );
            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('home-page');
            } else {
                $error = 'Ты не тот, за кого себя выдаешь';
            }

        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
    /**
     * @Route("/login/{afterDenied}", name="login")
     * @param null/string $afterDenied
     * @return Response
     */
    public function loginAction($afterDenied = null)
    {

        if ($afterDenied) {
            return new Response('Ошибка - доступа нет');
        }

        return new Response('доступа есть');
    }
}
