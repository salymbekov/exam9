<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RedditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'search',
                TextType::class
            )
            ->add(
                'sort', ChoiceType::class,[
                    'choices' => [
                        'релевантные' => 'relevance',
                        'горячие' => 'hot',
                        'в топе' => 'top',
                        'Новые' => 'new',
                        'Комментируемые' => 'comments'
                    ]
                ]
            )
            ->add(
                'limit', ChoiceType::class,[
                    'choices' => [
                        '8 шт' => '8',
                        '12 шт' => '12',
                        '16 шт' => '16',
                        '50 шт' => '50',
                        '100 шт' => '100'
                    ]
                ]
            )
            ->add('Search', SubmitType::class)
        ;
    }
}
