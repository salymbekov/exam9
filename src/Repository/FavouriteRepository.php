<?php

namespace App\Repository;

use App\Entity\Favourite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favourite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favourite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favourite[]    findAll()
 * @method Favourite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavouriteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Favourite::class);
    }

    /**
     * @param $user
     * @return Favourite[] Returns an array of Favourite objects
     */

    public function findByUser($user)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.user = :user')
            ->setParameter('user', $user)
            ->orderBy('f.dateTime', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * @param $user
     * @param int $limit
     * @param int $offset
     * @return Favourite|null
     */
    public function findByUserForPagination($user,  $offset, $limit = 2)
    {
        try {
            return $this->createQueryBuilder('f')
                ->where('f.user = :user')
                ->setParameter('user', $user)
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->orderBy('f.dateTime', 'DESC')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @param $user
     * @param $name
     * @return Favourite|null
     */
    public function findOneByUserAndName($user,  $name)
    {
        try {
            return $this->createQueryBuilder('f')
                ->where('f.user = :user')
                ->Andwhere('f.redditName = :name')
                ->setParameter('user', $user)
                ->setParameter('name', $name)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @param $data
     * @return Favourite|null
     */
    public function countPostsForUser($data)
    {
        try {
            return $this->createQueryBuilder('f')
                ->select('count(f.id) as qty')
                ->where('f.user = :user')
                ->Orwhere('f.redditName = :user')
                ->setParameter('user', $data)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @return Favourite|null
     */
    public function countAllPosts()
    {
        return $this->createQueryBuilder('f')
            ->select('f.redditName, count(f.redditName) as qty')
            ->groupBy('f.redditName')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $offset
     * @param int $limit
     * @return Favourite|null
     */
    public function societyFavourite($offset, $limit = 3)
    {
        return $this->createQueryBuilder('f')
            ->select('f.redditName, COUNT(f.redditName) AS HIDDEN b')
            ->groupBy('f.redditName')
            ->orderBy('b', "desc")
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

}
