<?php

use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" на странице$/
     * @param $arg1
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit('/');
    }

    /**
     * @When /^я перехожу на страницу "([^"]*)"$/
     */
    public function яПерехожуНаСтраницу($page)
    {
        $this->clickLink($page);
    }

    /**
     * @When /^я прохожу авторизацию$/
     */
    public function яПрохожуАвторизацию()
    {
        $this->fillField('auth_email', 'dope@email.com');
        $this->fillField('auth_password', '123');
        $this->pressButton('auth_login');

    }

    /**
     * @When /^заполняю поля регистрации$/
     * @param \Behat\Gherkin\Node\TableNode $table
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function заполняюПоляРегистрации(TableNode $table)
    {
        $this->fillFields($table);
        $this->pressButton('register_save');

    }


    /**
     * @When /^выбераю доп параметры$/
     */
    public function выбераюДопПараметры()
    {
        $this->checkOption('cottage_services_1');

        $this->pressButton('cottage_Зарегистрировать');

    }

    /**
     * @When /^выполняю поиск по слову "([^"]*)"$/
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function выполняюПоискПоСлову($arg1)
    {
        $this->fillField('reddit_search', $arg1);
        $this->getSession()->getPage()->selectFieldOption('reddit_sort', 'в топе');

        $this->pressButton('reddit_Search');

    }

    /**
     * @When /^нажимаю на "([^"]*)"$/
     */
    public function нажимаюНа($arg1)
    {
        $this->clickLink($arg1);

    }

    /**
     * @When /^добавляю в избранное$/
     */
    public function добавляюВИзбранное()
    {
        $this->clickLink('Добавить в избранное');

    }
}